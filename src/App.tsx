import React from 'react';

const App = (): JSX.Element => (
  <h1>
    Hello, react!
  </h1>
);

export default App;
